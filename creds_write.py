import sys, getopt
from time import strftime
import json
import xml.etree.ElementTree as ET
from requests.adapters import HTTPAdapter
import logging

#############################################################
#############       Functions                   #############
#############################################################
# Creates SSN auth file with proper structure
def ssn_auth(config_filename):
    json_info = {'consoleCredentials': 
        {
            'grant': 'clientCredentials',    
            'clientId': client_id, 
            'clientSecret': client_secret
        }
    }
    try:
        print('Creating SSN auth file: ' + config_filename)
        with open(config_filename, "w") as catalog_file:
            catalog_file.write(json.dumps(json_info, sort_keys=True, indent=4))
    except BaseException as why:
        logging.error('SSN auth creation failure: ' + str(why))
        return False
        quit()
    return True

# Takes command line args and sets vars
def cmdread(argv,config_filename):
    client_id = ''
    client_secret = ''
    try:
      opts, args = getopt.getopt(argv,"hi:o:",["client_id=","client_secret="])
    except getopt.GetoptError:
        print ('creds_write.py --client_id=<id> --client_secret=<secret>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('creds_write.py --client_id=<id> --client_secret=<secret>')
            sys.exit()
        elif opt in ("--client_id", "--cid"):
            client_id = arg
        elif opt in ("--client_secret", "--csecret"):
            client_secret = arg
    # print ('Input file is', client_id)
    # print ('Output file is', client_secret)
    json_info = {'consoleCredentials': 
        {
            'grant': 'clientCredentials',    
            'clientId': client_id, 
            'clientSecret': client_secret
        }
    }
    try:
        print('Creating SSN auth file: ' + config_filename)
        with open(config_filename, "w") as catalog_file:
            catalog_file.write(json.dumps(json_info, sort_keys=True, indent=4))
    except BaseException as why:
        logging.error('SSN auth creation failure: ' + str(why))
        return False
        quit()
    return True



#############################################################
#############       Main Program                   ##########
#############################################################

def main_program():
    cmdread(sys.argv[1:],'c:/temp/ssn_auth.json')
    # Create ssn auth with customer deployment info
    # ssn_auth('c:/temp/ssn_auth.json', client_id, client_secret)

if __name__ == "__main__":
    main_program()
