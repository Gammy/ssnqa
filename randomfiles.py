import argparse
import os

def command_line_parse():
    parser = argparse.ArgumentParser()
    output_arg = parser.add_argument('--output', help='Path to write dummy data to')
    try:
        args = parser.parse_args()
    except Exception as why:
        if str(why) != '0':
            print('Command line error: ' + str(why))
        sys.exit(2)
    if args.output is not None:
        output_path = args.output
    else:
        raise argparse.ArgumentError(output_arg, message='Missing "--output" parameter')
    return output_path


def file_create(file_path):
    with open(file_path, 'wb') as fout:
        fout.write(os.urandom(1024))

def main():
    output_path = command_line_parse()
    if not os.path.isdir(output_path):
        print(f'Creating directory {output_path}')
        os.makedirs(output_path)
    for i in range(0, 10):
        file_path = os.path.join(output_path, f'testfile{i}')
        file_create(file_path)
    with open(os.path.join(output_path, 'zero-byte.txt'), 'w') as fp:
        pass
    print (f'Dummy files written to {output_path}')
    return True

if __name__ == "__main__":
    if not main():
        sys.exit(1)
